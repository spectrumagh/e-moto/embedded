# Lokalizacja Firmware

Repozytorium korzysta z git submodule, gdzie submodułem jest Firmware STM32

## Wymagane pakiety

Powinny się też znajować w zmiennej środowiskowej `$PATH`


- gcc-arm-none-eabi - toolchain dla stm32 arm
- make - narzędzie budowania, CubeMX generuje `makefile` używane przez wspomniane narzędzie
- cmake - automat do budowania (lepsza wersja powyższego). Wspierany przez VSCode oraz CubeIDE (>=1.14)
- ninja-build (opcjonalne) - `cmake` konfiguruje projekt do `make`, ale może też do `ninja`, która jest dużo szybsza w budowaniu całości
- gdb-multiarch - debugger. W nowszych wersjach jest on zamiast `arm-none-eabi-gdb`

W przypadku debugger-a może okazać się konieczna poniższa komenda

```bash
sudo ln -s /usr/bin/gdb-multiarch /usr/bin/arm-none-eabi-gdb
```
