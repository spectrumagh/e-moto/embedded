cmake_minimum_required(VERSION 3.22)

set(FwSrc 
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc_ex.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_can.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart.c"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart_ex.c"
)


set(FwInc 
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Inc"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/STM32L4xx_HAL_Driver/Inc/Legacy"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/CMSIS/Device/ST/STM32L4xx/Include"
    "${PROJ_PATH}/../firmware/STM32CubeL4/Drivers/CMSIS/Include"
)