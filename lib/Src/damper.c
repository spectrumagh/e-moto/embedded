#include "damper.h"

void Damper_init(Damper_t *damper, double R_max, double max_ADCVal, double a, double b){
  damper->R_max = R_max;
  damper->max_ADCVal = max_ADCVal;
  damper->a = a;
  damper->b = b;
  }

uint16_t damper_calculate_deflection(Damper_t *damper,uint16_t ADCVal){
  
  double ratio = (double)ADCVal/(damper->max_ADCVal);  // value red from ADC relative to maximum ADC value
  double R = ratio * damper->R_max;   // resistance
  double deflection = damper->a * R + damper->b;  // deflection distance in cm

  deflection *=100; // scaling deflection 100x
  
  return (uint16_t) deflection; //return deflection scaled by 100x
  
}
