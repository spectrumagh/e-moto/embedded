#ifndef __DAMPER_H__
#define __DAMPER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"

typedef struct
{
    double R_max;   	//total damper resistance 
    double max_ADCVal;     //max ADC value 
    double a;       //linear regresion coefficients
    double b;       //
}Damper_t;

void Damper_init(Damper_t *damper, double R_max, double Ucc, double a, double b);
uint16_t damper_calculate_deflection(Damper_t *damper,uint16_t ADCVal);

#ifdef __cplusplus
}
#endif
#endif